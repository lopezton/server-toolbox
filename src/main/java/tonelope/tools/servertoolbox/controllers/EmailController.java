package tonelope.tools.servertoolbox.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tonelope.tools.servertoolbox.model.SendEmailRequest;
import tonelope.tools.servertoolbox.services.email.EmailService;

@RestController
public class EmailController {

	@Autowired
	private EmailService emailService;
	
	@RequestMapping(value = "/{version}/api/email/send", method = RequestMethod.POST)
	public boolean getHomeView(@RequestBody @Valid SendEmailRequest sendEmailRequest) {
		System.out.println("hi");
		final SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(sendEmailRequest.getTo());
		msg.setFrom(sendEmailRequest.getFrom());
		msg.setSubject(sendEmailRequest.getSubject());
		
		final StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append(sendEmailRequest.getMessage());
		messageBuilder.append("\n\n");
		messageBuilder.append(sendEmailRequest.getFrom());
		messageBuilder.append("\n");
		messageBuilder.append(sendEmailRequest.getPhone());
		msg.setText(messageBuilder.toString());
		
		return this.emailService.sendEmail(msg);
	}
}
