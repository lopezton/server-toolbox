package tonelope.tools.servertoolbox.filters;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component("xOriginScriptingFilter")
public class XOriginScriptingFilter extends OncePerRequestFilter {
	
	public static final Logger LOG = LoggerFactory.getLogger(XOriginScriptingFilter.class);
	
	private Properties prop = new Properties();
	 
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final InputStream in = getClass().getResourceAsStream("/conf/env.properties");
        prop.load(in);
        in.close();
 
        final Set<String> allowedOrigins = new HashSet<String>(Arrays.asList (prop.getProperty("allowed.origins").split(",")));
        final StringBuilder originBuilder = new StringBuilder();
        for (String allowedOrigin : allowedOrigins) {
        	originBuilder.append(allowedOrigin);
        }
        LOG.debug("Found allowed origins: {}", originBuilder.toString());
        
        if(request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
        	final String originHeader = request.getHeader("Origin");
        	LOG.debug("Checking if {} is an allowed origin.", originHeader);
        	
            if(allowedOrigins.contains(request.getHeader("Origin"))) {
                response.addHeader("Access-Control-Allow-Origin", originHeader);
                LOG.debug("Origin {} is allowed.", originHeader);
            } else {
            	LOG.debug("Origin {} was not a registered allowed origin.", originHeader);
            }
 
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type, X-Requested-With");
            response.addHeader("Access-Control-Max-Age", "1800");
        }
 
        filterChain.doFilter(request, response);
    }
}