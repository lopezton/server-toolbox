package tonelope.tools.servertoolbox.dao.activity;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class HibernateDao<T> {

	public static Logger LOG = LoggerFactory.getLogger(HibernateDao.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public boolean save(T obj) {
		try {
			this.sessionFactory.getCurrentSession().save(obj);
		} catch (Exception e) {
			LOG.error("Error: ", e);
			return false;
		}
		return true;
	}
}
