/**
 * JSONUtils.java May 18, 2011
 */
package tonelope.tools.servertoolbox.utils;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * <p>
 * This utility class converts the {@link Object} into JSON format string & vice versa.
 * </p>
 * 
 * @author sarfaraz
 * @since May 18, 2011
 */
public abstract class JSONUtils {

    /**
     * {@link ObjectMapper} instance.
     */
    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * <p>
     * This method converts the {@link Object} to JSON format String.
     * </p>
     * 
     * @param object
     *            {@link Object} instance.
     * @return String JSON format string
     * @throws IOException
     *             Throws exception if any
     */
    public static String toJSON(final Object object) throws IOException {
        return mapper.writeValueAsString(object);
    }

    /**
     * <p>
     * This method converts the JSON format string into {@link Object}.
     * </p>
     * 
     * @param json
     *            JSON format string
     * @param clazz
     *            {@link Class} instance
     * @return Object {@link Object} instance
     * @throws IOException
     *             Throws exception if any.
     */
    public static <T> T toObject(final String json, final Class<T> clazz) throws IOException {
        return mapper.readValue(json, clazz);
    }
}