package tonelope.tools.servertoolbox.aspects;

import java.lang.annotation.Annotation;
import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import tonelope.tools.servertoolbox.annotations.Loggable;
import tonelope.tools.servertoolbox.dao.model.ActivityLog;
import tonelope.tools.servertoolbox.services.logging.LogService;
import tonelope.tools.servertoolbox.utils.JSONUtils;

@Aspect
public class ActivityAspect {

	public static final Logger LOG = LoggerFactory.getLogger(ActivityAspect.class);
	
	@Autowired
	private LogService logService;
	
	@After("execution(@tonelope.tools.servertoolbox.annotations.Loggable * *(..))")
	public void annotatedWithLoggable(JoinPoint joinPoint) {
		final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		final String methodName = signature.getMethod().getName();
		final Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
		Annotation[] methodAnnotations;
		try {
			methodAnnotations = joinPoint.getTarget().getClass().getMethod(methodName, parameterTypes).getAnnotations();
			
			String activity = null;
			for(Annotation annotation : methodAnnotations) {
				if (annotation instanceof Loggable) {
					activity = ((Loggable) annotation).value();
					break;
				}
			}

			final String info = JSONUtils.toJSON(joinPoint.getArgs());
			
			final ActivityLog activityLog = new ActivityLog();
			activityLog.setActivity(activity);
			activityLog.setInfo(info);
			activityLog.setTime(new Date());
			
			if (!this.logService.log(activityLog)) {
				LOG.warn("Failed to update log for {}('{}')", methodName, activity);
			}
		} catch (Exception e) {
			LOG.warn("Failed to log activity for {}", methodName, e);
		}
	}
}
