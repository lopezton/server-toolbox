package tonelope.tools.servertoolbox.services.email;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import tonelope.tools.servertoolbox.annotations.Loggable;

@Service
public class EmailServiceImpl implements EmailService {

	public static final String APPLICATION_EMAIL_ADDRESS = "no-reply@tonelope.com";

	private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Autowired
	private MailSender mailSender;

	@Override
	@Loggable("email")
	public boolean sendEmail(SimpleMailMessage msg) {
		LOG.debug("Attempting to send email [to: {}, subject=\"{}\"].", msg.getTo(), msg.getSubject());
		if (StringUtils.isEmpty(msg.getFrom())) {
			msg.setFrom(APPLICATION_EMAIL_ADDRESS);
		}
		try {
			this.mailSender.send(msg);
		} catch (Exception e) {
			LOG.error("Unable to send e-mail to '{}'.", msg.getTo(), e);
			return false;
		}
		LOG.debug("Successfully sent email [to: {}, subject=\"{}\"].", msg.getTo(), msg.getSubject());
		return true;
	}

	@Override
	public boolean sendEmail(String to, String subject, String message) {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(to);
		msg.setSubject(subject);
		msg.setText(message);
		return this.sendEmail(msg);
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
}
