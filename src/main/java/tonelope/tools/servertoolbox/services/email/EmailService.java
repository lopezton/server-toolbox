package tonelope.tools.servertoolbox.services.email;

import org.springframework.mail.SimpleMailMessage;

/**
 * EmailService.java
 * 
 * @author tlopez
 * 
 */
public interface EmailService {

	/**
	 * <p>
	 * Sends an email using the appropriate paramaters.
	 * </p>
	 * 
	 * @param msg
	 *            the <tt>SimpleMailMessage</tt> object to send.
	 * @return Returns 1 if successful, -1 otherwise.
	 */
	boolean sendEmail(SimpleMailMessage msg);

	/**
	 * <p>
	 * Sends an email using the appropriate paramaters.
	 * </p>
	 * 
	 * @param to
	 *            E-mail address of the sendee.
	 * @param subject
	 *            Subject of the e-mail address.
	 * @param message
	 *            Message of the e-mail address.
	 * @return Returns 1 if successful, -1 otherwise.
	 */
	boolean sendEmail(String to, String subject, String message);
}
