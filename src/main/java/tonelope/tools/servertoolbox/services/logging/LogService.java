package tonelope.tools.servertoolbox.services.logging;

import tonelope.tools.servertoolbox.dao.model.ActivityLog;

public interface LogService {

	boolean log(ActivityLog log);
}
