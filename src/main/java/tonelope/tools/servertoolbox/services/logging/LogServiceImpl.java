package tonelope.tools.servertoolbox.services.logging;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tonelope.tools.servertoolbox.dao.activity.ActivityLogDao;
import tonelope.tools.servertoolbox.dao.model.ActivityLog;

@Service
@Transactional
public class LogServiceImpl implements LogService {

	@Autowired
	private ActivityLogDao activityLogDao;
	
	@Override
	public boolean log(ActivityLog log) {
		return this.activityLogDao.save(log);
	}

}
