package tonelope.tools.servertoolbox.interceptors;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class XOriginRequestScriptingInterceptor extends HandlerInterceptorAdapter {
	
	public static final Logger LOG = LoggerFactory.getLogger(XOriginRequestScriptingInterceptor.class);
	
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final InputStream in = getClass().getResourceAsStream("/conf/env.properties");
        final Properties prop = new Properties();
        prop.load(in);
        in.close();
 
        final Set<String> allowedOrigins = new HashSet<String>(Arrays.asList(prop.getProperty("allowed.origins").split(",")));
 
        final String origin = request.getHeader("Origin");
        if(allowedOrigins.contains(origin)) {
            LOG.debug("{} is an accepted origin.", origin);
        	response.addHeader("Access-Control-Allow-Origin", origin);
        }
        return true;
    }
}
