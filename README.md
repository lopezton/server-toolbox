# Server Toolbox
This repository holds the code base for the Server Toolbox project. This project contains a set of server utility functions for
use by http://tonelope.com and it's subdomains.

## Project Technologies
This project is written in Java/Spring. The project includes use of the following technologies:  

 * Maven 3
 * Java 7
 * Tomcat 7
 * Spring MVC
 * Hibernate Validation
 * Ehcache
